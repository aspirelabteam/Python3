#girilen sayının tek yada çift olduğunu bulan programı yazınız.

deger = int(input("Lütfen bir sayı giriniz :"))

if(deger % 2 == 0):
    print("Girilen {} sayısı çift bir sayıdır.".format(deger))
else:
    print("Girilen {} sayısı tek bir sayıdır.".format(deger))
